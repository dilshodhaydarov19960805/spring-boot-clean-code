package uz.pdp.springbootcleancode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringBootCleanCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCleanCodeApplication.class, args);

    }
    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
