package uz.pdp.springbootcleancode.model;

import lombok.Getter;
import lombok.Setter;
import uz.pdp.springbootcleancode.domain.Comment;

import java.util.List;

@Setter
@Getter
public class ResponseCommentList {
    private List<Comment> comments ;
    private boolean success ;

    public ResponseCommentList(ResponseCommentList responseCommentList) {
        this.comments = responseCommentList.comments;
        this.success = responseCommentList.success;
    }

    public ResponseCommentList() {

    }
}
