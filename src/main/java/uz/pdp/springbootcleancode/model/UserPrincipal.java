package uz.pdp.springbootcleancode.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.pdp.springbootcleancode.domain.Users;
import java.util.List;

public class UserPrincipal implements UserDetails {
    private String userName;
    private String password;
    private List<GrantedAuthority> authorities;
    private String fullName;
    private Users user;

    public UserPrincipal(String userName, String password, List<GrantedAuthority> authorities, String fullName, Users user) {
        this.userName = userName;
        this.password = password;
        this.authorities = authorities;
        this.fullName = fullName;
        this.user = user;
    }
    public UserPrincipal() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
