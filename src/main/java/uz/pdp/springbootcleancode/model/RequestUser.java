package uz.pdp.springbootcleancode.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestUser {
    @NotNull
    private String userName;

    @Email
    private String email;
    @NotNull
    private String prePassword;

    @NotNull
    private String password;

    @NotNull
    private String fullName;
    @NotNull
    private Integer age;
    @NotNull
    private String phoneNumber;
    @AssertTrue(message = "Password not confirmed")
    public boolean isValid(){
        return this.password.equals(this.prePassword);
    }

}
