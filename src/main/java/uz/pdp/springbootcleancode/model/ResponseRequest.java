package uz.pdp.springbootcleancode.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Setter
@Getter
public class ResponseRequest {
    private boolean success;
    private String status;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Object data;


    public ResponseRequest() {

    }

    public ResponseRequest(ResponseRequest responseRequest) {
        this.status = responseRequest.status;
        this.success = responseRequest.success;
        this.data = responseRequest.data;
    }
}
