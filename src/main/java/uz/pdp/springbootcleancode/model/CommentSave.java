package uz.pdp.springbootcleancode.model;
import lombok.Data;

@Data
public class CommentSave {
    private Integer postId;
    private String fullNames;
    private String text;
    private Long userId;
}
