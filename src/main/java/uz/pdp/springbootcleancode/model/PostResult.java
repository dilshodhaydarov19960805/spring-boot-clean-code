package uz.pdp.springbootcleancode.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostResult {
    private String name;
    private String text;
    private Long postId;
    private Long imgUri;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern="yyyy-mm-dd")
    private Date createAt;
}
