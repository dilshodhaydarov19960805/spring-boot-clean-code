package uz.pdp.springbootcleancode.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuggestionUser {
    private Long userId;
    private Long userIdSuggestion;
    private Integer statusValue;
}
