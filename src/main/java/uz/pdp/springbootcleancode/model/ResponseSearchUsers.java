package uz.pdp.springbootcleancode.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Setter
@Getter
public class ResponseSearchUsers {
    private Set<UsersSearch> data;

    public ResponseSearchUsers(Set<UsersSearch> data) {
        this.data = data;
    }
}
