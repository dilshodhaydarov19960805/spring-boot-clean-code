package uz.pdp.springbootcleancode.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UsersSearch {
    private String fullName;
    private Long userId;
    private Integer status;
}
