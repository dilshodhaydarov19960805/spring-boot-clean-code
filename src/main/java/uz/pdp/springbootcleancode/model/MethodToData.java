package uz.pdp.springbootcleancode.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.springbootcleancode.domain.Images;
import uz.pdp.springbootcleancode.domain.Post;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.service.ImageService;
import uz.pdp.springbootcleancode.service.PostService;
import uz.pdp.springbootcleancode.service.UsersService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
@Component
public class MethodToData {
    private final UsersService usersService;
    @Value("${file.path}")
    private String uploadFileUri ;
    @Autowired
    private ImageService imageService;
    @Autowired
    private PostService postService;

    public MethodToData(UsersService usersService) {
        this.usersService = usersService;
    }

    public String getUploadPathFile(MultipartFile multipartFile) throws IOException {
        byte [] bytes =multipartFile.getBytes();

        Path path = Paths.get(uploadFileUri +"/"+ multipartFile.getOriginalFilename());
        String uri = uploadFileUri + "/" +multipartFile.getOriginalFilename();
        Files.write(path,bytes);
        return uri;
    }


    public String getUploadFileType(String fileName){
        String ext = null;
        if (fileName != null && !fileName.isEmpty()){
            int dot = fileName.lastIndexOf(".");
            if (dot > 0 && dot <= fileName.length()-2){
                ext = fileName.substring((dot + 1));
            }
        }
        return ext;

    }


    public ResponseRequest setPostSave(MultipartFile multipartFile, String text, String userId) throws IOException {
        ResponseRequest responseRequest = new ResponseRequest();
        if (text.length() > 0 || !multipartFile.isEmpty()) {
            Users users = usersService.findById(Long.valueOf(userId));
            Post post = new Post();
            post.setUser(users);
            post.setTitle(text);
            post.setCreateAt(new Date());
            Long id = null;
            if (!multipartFile.isEmpty()) {
                Images images = new Images();
                getUploadPathFile(multipartFile);
                images.setType(getUploadFileType(multipartFile.getOriginalFilename()));
                images.setUploadPath(getUploadPathFile(multipartFile));
                images.setName(multipartFile.getOriginalFilename());
                images.setContentType(multipartFile.getContentType());
                post.setImages(imageService.save(images));
                id = postService.save(post);
            } else {
                postService.save(post);
            }
            PostResult postResult = new PostResult();
            postResult.setName(users.getFullName());
            postResult.setText(text);
            postResult.setCreateAt(post.getCreateAt());
            postResult.setPostId(id);
            if (postService.getPostImageId(id) !=null){
                postResult.setImgUri(postService.getPostImageId(id));
            }else {
                postResult.setImgUri(Long.valueOf(0));
            }
            responseRequest.setSuccess(true);
            responseRequest.setStatus("success");
            responseRequest.setData(postResult);
            return new uz.pdp.springbootcleancode.model.ResponseRequest(responseRequest);

        }
        PostResult postResult = new PostResult();
        postResult.setText("post empty.");
        responseRequest.setSuccess(false);
        responseRequest.setStatus("error");
        responseRequest.setData(postResult);

        return new uz.pdp.springbootcleancode.model.ResponseRequest(responseRequest);
    }
}
