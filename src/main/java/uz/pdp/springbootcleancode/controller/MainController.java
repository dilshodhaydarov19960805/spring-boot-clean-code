package uz.pdp.springbootcleancode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.model.*;
import uz.pdp.springbootcleancode.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

@Controller
public class MainController {
    private final UsersService usersService;
    private final PostService postService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private AuthService authService;
    @Autowired
    private AuthenticationManager authenticationManager;

    public MainController(UsersService usersService, PostService postService) {
        this.usersService = usersService;
        this.postService = postService;
    }

    @GetMapping("/")
    public String index(){
        return "index";
    }

    @GetMapping("/login")
    public String signIn(){
        if (authService.loginAuth()){
            return "login";
        }else {

            return "redirect:/cabinet";
        }
    }

    @GetMapping("/sign_up_save")
    public String getRegister(RequestUser requestUser, Model model) {

        if (authService.loginAuth()) {
            model.addAttribute("requestUser", requestUser);
            return "signUp";
        } else {
            return "redirect:/sign_up_save";
        }
    }

    @PostMapping("/sign_up_save")
    public String signUp(@Valid  RequestUser requestUser, BindingResult bindingResult, Model model, HttpServletRequest request) throws IOException {
        if (bindingResult.hasErrors()) {
            return "signUp";
        } else {
            Response response = authService.register(requestUser);
            if (response.isSuccess()) {
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(requestUser.getUserName(), requestUser.getPassword());
                token.setDetails(new WebAuthenticationDetails(request));
                Authentication authentication = authenticationManager.authenticate(token);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return "redirect:/cabinet";
            } else if (response.getMassage().equals("userName")){
                model.addAttribute("massage",requestUser.getUserName()+" already exists.");
            }else {
                model.addAttribute("massage","Password not confirmed");
            }
        }
        return "signUp";
    }

    @GetMapping("/cabinet")

    public String cabinet( @CurrentUser UserPrincipal userPrincipal,Model model){

        Users users = usersService.findByUserName(userPrincipal.getUserName());
        model.addAttribute("fullName",userPrincipal.getFullName());
        model.addAttribute("userId",users.getUserId());
        model.addAttribute("postLists", postService.findAll(users.getUserId()));
        model.addAttribute("comments",commentService.findAll());
        model.addAttribute("images",imageService.findAll());
//        model.addAttribute("searchUsers",usersService.findFullNameSearchValue(users.getUserId()));
        model.addAttribute("userSuggestions",usersService.getUserSuggestion(users.getUserId()));
        model.addAttribute("myFriends",usersService.getMyFriends(users.getUserId()));
        return "Cabinet";
    }


}
