package uz.pdp.springbootcleancode.controller;


import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.springbootcleancode.domain.Comment;
import uz.pdp.springbootcleancode.model.*;
import uz.pdp.springbootcleancode.service.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PostAndCommentRestController {
    private final UsersService usersService;
    private final ConnectUsersService connectUsersService;
    private final ImageService imageService;
    private final CommentService commentService;
    private final MethodToData methodToData;
    public PostAndCommentRestController(UsersService usersService, ConnectUsersService connectUsersService, ImageService imageService, CommentService commentService, MethodToData methodToData) {
        this.usersService = usersService;
        this.connectUsersService = connectUsersService;
        this.imageService = imageService;
        this.commentService = commentService;
        this.methodToData = methodToData;
    }

    // COMMENT SAVE METHOD //

    @PostMapping("/commentSave")
    public ResponseRequest comment(
            @RequestBody CommentSave commentSave) throws ParseException {
        CommentResult commentResult = new CommentResult();
        ResponseRequest responseRequest = new ResponseRequest();
        if (!commentSave.getText().equals("")){
        Comment comment = commentService.save(commentSave);
        if ( comment != null) {
            commentResult.setFullName(commentSave.getFullNames());
            commentResult.setTextValue(commentSave.getText());
            commentResult.setDate(comment.getCreateAt());
            responseRequest.setSuccess(true);
            responseRequest.setStatus("success");
            responseRequest.setData(commentResult);
            return new uz.pdp.springbootcleancode.model.ResponseRequest(responseRequest);
        }}

        responseRequest.setSuccess(false);
        responseRequest.setStatus("error");
        responseRequest.setData(commentResult);
        return new uz.pdp.springbootcleancode.model.ResponseRequest(responseRequest);
    }
//    POST SAVE METHOD

    @PostMapping(value = "/post", consumes = "*/*")

    public uz.pdp.springbootcleancode.model.ResponseRequest post(
                                                            @RequestParam("file") MultipartFile multipartFile,
                                                            @RequestParam("text") String text,
                                                            @RequestParam("userId")String userId) throws IOException {
        return methodToData.setPostSave(multipartFile,text,userId);
    }


    @GetMapping("/image/{id}")
    public void readImageAndDisplay(
            @PathVariable Long id, HttpServletResponse response) throws Throwable {
        if (id != 0){
            response.setContentType(imageService.getContentType(id));
            IOUtils.copy(imageService.idAll(id), response.getOutputStream());
        }

    }
    @PostMapping("/suggestion")
    public ResponseRequest suggestion(@Valid @RequestBody SuggestionUser suggestionUser){

        return connectUsersService.save(suggestionUser);
    }

    @PostMapping("/postCommentAll" )
    public ResponseCommentList commentList(@RequestParam("postId") Long id) {
        ResponseCommentList responseCommentList = new ResponseCommentList();
        List<Comment> commentList = commentService.postCommentList(id);
        if (!commentList.isEmpty()) {
            responseCommentList.setSuccess(true);
            responseCommentList.setComments(commentList);
            return new ResponseCommentList(responseCommentList);
        }else {
            responseCommentList.setSuccess(false);
            responseCommentList.setComments(null);
            return new ResponseCommentList(responseCommentList);
        }
    }

    @PostMapping("/searchUser")
    public ResponseSearchUsers search(@RequestParam("value") String value,
                                        @RequestParam("userId") Long id ) {

        return usersService.findFullNameSearchValue(id,value);
    }
}


