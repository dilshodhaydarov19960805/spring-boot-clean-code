package uz.pdp.springbootcleancode.domain;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Images implements Serializable {

    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long imageId;

    @Column(name = "upload_path")
    private String uploadPath;

    @Column(name = "type")
    private String type;

    @Column (name = "name")
    private String name;

    @Column (name = "content_type")
    private String contentType;

}