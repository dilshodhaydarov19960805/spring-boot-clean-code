package uz.pdp.springbootcleancode.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Setter
@Getter
public class ConnectUsers implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "connect_user_id")
    private Long connectUserId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_id",referencedColumnName = "user_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Users froms;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_id",referencedColumnName = "user_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Users to;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    @Column(name = "create_at")
    private Date createAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_id",referencedColumnName = "status_id")
    private Status status;


}
