package uz.pdp.springbootcleancode.constant;

public final class UsersStateConstant {

    public static final int FRIEND_SUCCESS = 30;
    public static final int FRIEND_TO_OFFER = 10;
    public static final int FRIEND_REJECT = 60;

}
