package uz.pdp.springbootcleancode.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.springbootcleancode.domain.Images;

@Repository
public interface ImageRepository extends JpaRepository<Images,Long> {
}
