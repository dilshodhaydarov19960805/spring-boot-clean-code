package uz.pdp.springbootcleancode.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.springbootcleancode.domain.Users;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users,Long> {
    boolean existsByUserName(String userName);

    @Query(value = "SELECT * FROM users_face u WHERE lower(u.full_name)  LIKE %:val%", nativeQuery = true)
    List<Users> searchUserAll(@Param("val") String val);

    Optional<Users> findByUserName(String userName);
    Users findByFullName(String fullName);

    @Query(value = "select * from users_face u where u.user_name = ?1",nativeQuery = true)
    Users findByuserName(String userName);

    @Query(value = "insert into users_face(user_name, age, full_name, email, password, phone_number)" +
            "values (:#{#user.userName},:#{#user.age},:#{#user.fullName},:#{#user.email}" +
            ",:#{#user.password},:#{#user.phoneNumber})",nativeQuery = true)
    Boolean saveUser(@Param("user") Users users);
}
