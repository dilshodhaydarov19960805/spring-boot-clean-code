package uz.pdp.springbootcleancode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.springbootcleancode.domain.Post;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {
Post findByPostId(Long postId);
}
