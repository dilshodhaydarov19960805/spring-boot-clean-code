package uz.pdp.springbootcleancode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.springbootcleancode.domain.ConnectUsers;
import uz.pdp.springbootcleancode.domain.Status;
import uz.pdp.springbootcleancode.domain.Users;

@Repository
public interface ConnectUsersRepository extends JpaRepository<ConnectUsers,Long> {

    boolean existsByStatusAndToAndFroms(Status status, Users to, Users froms);

    ConnectUsers findByFromsAndTo(Users one, Users one1);
    ConnectUsers findByStatusAndFromsAndTo(Status status, Users users, Users users1);
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM connect_users u WHERE u.connect_user_id =?1",nativeQuery = true)
    void deleteRejectUser( Long id);
    boolean existsByToAndStatus(Users users,Status status);
    boolean existsByFromsAndStatus(Users users,Status status);
}
