package uz.pdp.springbootcleancode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.pdp.springbootcleancode.domain.Comment;
import uz.pdp.springbootcleancode.domain.Post;
import uz.pdp.springbootcleancode.domain.Users;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
List<Comment> findByPost(Post post);
}
