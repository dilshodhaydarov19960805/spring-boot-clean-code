package uz.pdp.springbootcleancode.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.springbootcleancode.domain.Status;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
    Status findByStatusValue(Integer value);
}
