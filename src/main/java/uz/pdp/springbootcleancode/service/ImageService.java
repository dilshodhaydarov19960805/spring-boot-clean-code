package uz.pdp.springbootcleancode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.domain.Images;
import uz.pdp.springbootcleancode.repository.ImageRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Value("${file.path}")
    private static String filePath;

    public Images save(Images images){
//        images.setUploadPath(filePath + "/" + images.getName());
        return imageRepository.save(images);
    }

    public List<Images> findAll() {
        return imageRepository.findAll();
    }

    public InputStream idAll(Long id) throws FileNotFoundException {
        Optional<Images> image = imageRepository.findById(id);
        if (image.isPresent()) {
            File file = new File(image.get().getUploadPath());

            InputStream in = new FileInputStream(file);


            return in;
        }
        return null;
    }

    public String getContentType(Long id) {
        Optional<Images> images = imageRepository.findById(id);
        if (images.isPresent()){
            File file = new File(images.get().getContentType());
            return String.valueOf(file);
        }
        return "*/*";
    }
}
