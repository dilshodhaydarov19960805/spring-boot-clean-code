package uz.pdp.springbootcleancode.service;

import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.domain.Images;
import uz.pdp.springbootcleancode.domain.Post;
import uz.pdp.springbootcleancode.domain.Status;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.repository.ConnectUsersRepository;
import uz.pdp.springbootcleancode.repository.PostRepository;
import uz.pdp.springbootcleancode.repository.StatusRepository;
import uz.pdp.springbootcleancode.repository.UsersRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PostService {
    private final PostRepository postRepository;
    private final StatusRepository statusRepository;
    private final ConnectUsersRepository connectUsersRepository;
    private final UsersRepository usersRepository;
    public PostService(PostRepository postRepository, StatusRepository statusRepository, ConnectUsersRepository connectUsersRepository, UsersRepository usersRepository) {
        this.postRepository = postRepository;
        this.statusRepository = statusRepository;
        this.connectUsersRepository = connectUsersRepository;
        this.usersRepository = usersRepository;
    }
    public Set<Post> findAll(Long id){
        Users users =usersRepository.getOne(id);
        List<Post> postList = postRepository.findAll();
        Images images = new Images();
        images.setImageId(0l);
        Set<Post>postSet = new HashSet<>();
        Status status = statusRepository.findByStatusValue(30);
        postList.forEach(post -> {
            Users users1 = usersRepository.getOne(post.getUser().getUserId());
            if (connectUsersRepository.existsByStatusAndToAndFroms(status, users, users1) || connectUsersRepository.existsByStatusAndToAndFroms(status, users1, users)) {
                if (post.getImages() == null){
                    post.setImages(images);
                    postSet.add(post);
                }
                postSet.add(post);
            } else if (users1 == users) {
                if (post.getImages() == null){
                    post.setImages(images);
                    postSet.add(post);
                }
                postSet.add(post);
            }
        });
        return postSet;
    }

    public Post findById(Long postId) {

        return postRepository.findByPostId(postId);
    }

    public Long save(Post post) {
        Post post1 = postRepository.save(post);
        Long id = post1.getPostId();
        return id;
    }

    public Long getPostImageId(Long id) {
        if (id != null){
            Post post = postRepository.findByPostId(id);
            Long postImageId = post.getImages().getImageId();
            return postImageId;

        }else {
            return null;
        }
    }
}
