package uz.pdp.springbootcleancode.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.domain.Comment;
import uz.pdp.springbootcleancode.domain.Post;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.model.CommentSave;
import uz.pdp.springbootcleancode.model.MethodToData;
import uz.pdp.springbootcleancode.repository.CommentRepository;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    private final UsersService usersService;
    private final PostService postService;
    private final MethodToData methodToData;
    public CommentService(UsersService usersService, PostService postService, MethodToData methodToData) {
        this.usersService = usersService;
        this.postService = postService;
        this.methodToData = methodToData;
    }

    public List<Comment> findAll(){

        return commentRepository.findAll();
    }

    public Comment save(CommentSave commentSave) throws ParseException {

        Users users = usersService.findById(commentSave.getUserId());
        Comment comment = new Comment();
        Post post = postService.findById(Long.valueOf(commentSave.getPostId()));
        comment.setUser(users);
        comment.setPost(post);
        comment.setText(commentSave.getText());
        comment.setCreateAt(new Date());
        return commentRepository.save(comment);
    }


    public List<Comment> postCommentList(Long id) {
        Post post = postService.findById(id);
        return commentRepository.findByPost(post);

    }
}
