package uz.pdp.springbootcleancode.service;

import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.domain.ConnectUsers;
import uz.pdp.springbootcleancode.domain.Status;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.model.ResponseSearchUsers;
import uz.pdp.springbootcleancode.model.UsersSearch;
import uz.pdp.springbootcleancode.repository.ConnectUsersRepository;
import uz.pdp.springbootcleancode.repository.StatusRepository;
import uz.pdp.springbootcleancode.repository.UsersRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UsersService {
    private final StatusRepository statusRepository;
    private final UsersRepository usersRepository;
    private final ConnectUsersRepository connectUsersRepository;
    public UsersService(StatusRepository statusRepository, UsersRepository usersRepository, ConnectUsersRepository connectUsersRepository) {
        this.statusRepository = statusRepository;
        this.usersRepository = usersRepository;
        this.connectUsersRepository = connectUsersRepository;
    }
    public boolean findUserName(String userName){
        return usersRepository.existsByUserName(userName);
    }

    public Users findFullName(String fullName) {
        return usersRepository.findByFullName(fullName);
    }

    public Users save(Users users) {
        return usersRepository.save(users);

    }

    public Users findByUserName(String userName) {
        return usersRepository.findByuserName(userName);
    }

    public Users findById(Long id) {
        return usersRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("invalid user id: "+id));

    }

    public Set<Users> findAll(Long id) {

        List<Users> users = usersRepository.findAll();
        List<ConnectUsers> connectUsers = connectUsersRepository.findAll();
        Set<Users> usersList = new HashSet<>();
        List<Users> longList = new ArrayList<>();
       if (connectUsers.isEmpty()){
           connectUsers.forEach(connectUsers1 -> {
               if (id == connectUsers1.getFroms().getUserId()){
                   longList.add(connectUsers1.getTo());
               }
           });
           users.forEach(users1 -> {
               if (id != users1.getUserId()) {
                   longList.forEach(aLong -> {
                       if (!users1.getUserId().equals(aLong)){
                           usersList.add(users1);
                       }
                   });

               }
           });
       }
        users.forEach(users1 -> {
            if (id != users1.getUserId()) {
                        usersList.add(users1);
                    }
                });
        return usersList;
    }

    public Set<Users> getUserSuggestion(Long userId) {
        Set<Users> usersSet = new HashSet<>();
        List<ConnectUsers> connectUsers = connectUsersRepository.findAll();
        connectUsers.forEach(connectUsers1 -> {
            if (userId.equals(connectUsers1.getTo().getUserId()) && connectUsers1.getStatus().getStatusValue() == 10) {
                usersSet.add(connectUsers1.getFroms());
            }
        });
        return usersSet;
    }

    public Set<Users> getMyFriends(Long id) {
        Set<Users> usersList = new HashSet<>();
        List<ConnectUsers> connectUsersList = connectUsersRepository.findAll();
        connectUsersList.forEach(connectUsers -> {
            if (connectUsers.getTo().getUserId()==id && connectUsers.getStatus().getStatusValue() == 30 ) {
                usersList.add(connectUsers.getFroms());
            }
            if (connectUsers.getFroms().getUserId() ==id && connectUsers.getStatus().getStatusValue() == 30){
                usersList.add(connectUsers.getTo());
            }
        });
        return usersList;
    }

    public ResponseSearchUsers findFullNameSearchValue(Long id,String val) {
        Set<UsersSearch> usersSearches = new HashSet<>();
        Users users1= usersRepository.getOne(id);
        Status status = statusRepository.findByStatusValue(10);
        Status status1 = statusRepository.findByStatusValue(30);

        List<Users> usersList = usersRepository.searchUserAll(val);
        usersList.forEach(users -> {
            if (connectUsersRepository.existsByStatusAndToAndFroms(status, users, users1) || connectUsersRepository.existsByStatusAndToAndFroms(status, users1, users)) {
                UsersSearch usersSearch = new UsersSearch();
                usersSearch.setUserId(users.getUserId());
                usersSearch.setFullName(users.getFullName());
                usersSearch.setStatus(10);
                usersSearches.add(usersSearch);
            } else if (connectUsersRepository.existsByStatusAndToAndFroms(status1, users, users1) || connectUsersRepository.existsByStatusAndToAndFroms(status1, users1, users)) {
                UsersSearch usersSearch = new UsersSearch();
                usersSearch.setUserId(users.getUserId());
                usersSearch.setFullName(users.getFullName());
                usersSearch.setStatus(30);
                usersSearches.add(usersSearch);
            } else {
                UsersSearch usersSearch = new UsersSearch();
                usersSearch.setUserId(users.getUserId());
                usersSearch.setFullName(users.getFullName());
                usersSearch.setStatus(60);
                usersSearches.add(usersSearch);
            }
        });
        return new ResponseSearchUsers(usersSearches);
    }
}
