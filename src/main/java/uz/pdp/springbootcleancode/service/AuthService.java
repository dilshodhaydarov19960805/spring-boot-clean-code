package uz.pdp.springbootcleancode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.domain.Images;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.model.*;
import uz.pdp.springbootcleancode.repository.UsersRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UsersRepository usersRepository;
    private final ImageService imageService;
    private final MethodToData methodToData;

    public AuthService(ImageService imageService, MethodToData methodToData) {
        this.imageService = imageService;
        this.methodToData = methodToData;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Users users = usersRepository.findByUserName(s).orElseThrow(()-> new UsernameNotFoundException(s));

        List<GrantedAuthority> authorities = new ArrayList<>();
        return new UserPrincipal(
                users.getUserName(),
                users.getPassword(),
                authorities,
                users.getFullName(),
                users
        );
    }

    public boolean loginAuth() {
        return (SecurityContextHolder.getContext().getAuthentication()
                instanceof AnonymousAuthenticationToken);
    }

    public Response register(RequestUser requestUser) throws IOException {
        PasswordEncoder passwordEncoder  =new BCryptPasswordEncoder();
        Response response = new Response();
        if (requestUser.isValid()) {
            try {
                usersRepository.save(
                        new Users(requestUser.getFullName(),
                                requestUser.getPhoneNumber(),
                                requestUser.getAge(),
                                requestUser.getEmail(),
                                requestUser.getUserName(),
                                passwordEncoder.encode(requestUser.getPassword())

                        ));

                response.setSuccess(true);
                response.setMassage("Successfully");
            } catch (Exception e) {
                response.setSuccess(false);
                response.setMassage("userName");
            }
        } else {
            response.setSuccess(false);
            response.setMassage("password");
        }
        return response;
    }


}
