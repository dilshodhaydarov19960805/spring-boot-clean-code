package uz.pdp.springbootcleancode.service;

import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.repository.StatusRepository;

@Service
public class StatusService {
    private final StatusRepository statusRepository;

    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }
}
