package uz.pdp.springbootcleancode.service;

import org.springframework.stereotype.Service;
import uz.pdp.springbootcleancode.domain.ConnectUsers;
import uz.pdp.springbootcleancode.domain.Status;
import uz.pdp.springbootcleancode.domain.Users;
import uz.pdp.springbootcleancode.model.ResponseRequest;
import uz.pdp.springbootcleancode.model.SuggestionUser;
import uz.pdp.springbootcleancode.repository.ConnectUsersRepository;
import uz.pdp.springbootcleancode.repository.StatusRepository;
import uz.pdp.springbootcleancode.repository.UsersRepository;

import java.util.Date;

@Service
public class ConnectUsersService {
    private final ConnectUsersRepository connectUsersRepository;
    private final StatusRepository statusRepository;
    private final UsersRepository  usersRepository;
    public ConnectUsersService(ConnectUsersRepository connectUsersRepository, StatusRepository statusRepository, UsersRepository usersRepository) {
        this.connectUsersRepository = connectUsersRepository;
        this.statusRepository = statusRepository;
        this.usersRepository = usersRepository;
    }

    public ResponseRequest save(SuggestionUser suggestionUser) {
        Users users1 = usersRepository.getOne(suggestionUser.getUserId());
        Users users = usersRepository.getOne(suggestionUser.getUserIdSuggestion());
        ConnectUsers connectUsers = new ConnectUsers();
        ResponseRequest responseRequest = new ResponseRequest();

        if (suggestionUser.getStatusValue() == 10) {
            Status status = statusRepository.findByStatusValue(10);
            boolean connectUsers1 = connectUsersRepository.existsByStatusAndToAndFroms(status, users, users1);
            if (!connectUsers1) {
                ConnectUsers connectUsers2 = connectUsersRepository.findByFromsAndTo(users, users1);
                if (connectUsers2 == null) {
                    ConnectUsers connectUsers3 = connectUsersRepository.findByFromsAndTo(users1,users);
                    if (connectUsers3 ==null){
                        connectUsers.setFroms(users1);
                        connectUsers.setTo(users);
                        connectUsers.setStatus(status);
                        connectUsers.setCreateAt(new Date());
                        connectUsersRepository.save(connectUsers);
                        responseRequest.setSuccess(true);
                        responseRequest.setStatus("The request has been sent");
                        responseRequest.setData(connectUsers);
                        return new ResponseRequest(responseRequest);
                    }else {
                        responseRequest.setSuccess(false);
                        responseRequest.setStatus("The offer was accepted");
                        responseRequest.setData(connectUsers);
                        return new ResponseRequest(responseRequest);

                    }
                } else if (connectUsers2.getStatus().getStatusValue() == 30) {
                    responseRequest.setSuccess(false);
                    responseRequest.setStatus("The offer was accepted");
                    responseRequest.setData(connectUsers);
                    return new ResponseRequest(responseRequest);
                }
            }
        } else if (suggestionUser.getStatusValue() == 30) {
            Status status = statusRepository.findByStatusValue(30);
            ConnectUsers connectUsers1 = connectUsersRepository.findByFromsAndTo(users, users1);
            connectUsers1.setStatus(status);
            connectUsersRepository.save(connectUsers1);
            responseRequest.setSuccess(true);
            responseRequest.setData(users);
            return new ResponseRequest(responseRequest);
        } else if (suggestionUser.getStatusValue() == 60) {
            Status status = statusRepository.findByStatusValue(10);
            ConnectUsers connectUsers1 = connectUsersRepository.findByStatusAndFromsAndTo(status, users, users1);
            connectUsersRepository.deleteRejectUser(connectUsers1.getConnectUserId());
            responseRequest.setSuccess(true);
            responseRequest.setStatus("The request was denied");
            responseRequest.setData(connectUsers);
            return new ResponseRequest(responseRequest);
        } else if (suggestionUser.getStatusValue() == 12){

            Status status = statusRepository.findByStatusValue(10);
            ConnectUsers connectUsers1 = connectUsersRepository.findByStatusAndFromsAndTo(status, users1, users);
            if (connectUsers1 != null) {
                connectUsersRepository.deleteRejectUser(connectUsers1.getConnectUserId());
                responseRequest.setSuccess(true);
                responseRequest.setStatus("The request was canceled");
                responseRequest.setData(connectUsers);
                return new ResponseRequest(responseRequest);
            }else {
                Status status1 = statusRepository.findByStatusValue(30);
                ConnectUsers connectUsers2 = connectUsersRepository.findByStatusAndFromsAndTo(status1, users1, users);
                if (connectUsers2 != null){
                    responseRequest.setSuccess(false);
                    responseRequest.setStatus("The offer was accepted");
                    responseRequest.setData(connectUsers);
                    return new ResponseRequest(responseRequest);
                }
            }
        }else if (suggestionUser.getStatusValue() == 70){
            Status status = statusRepository.findByStatusValue(30);
            ConnectUsers connectUsers1 = connectUsersRepository.findByStatusAndFromsAndTo(status, users, users1);
            if (connectUsers1 != null) {
                connectUsersRepository.deleteRejectUser(connectUsers1.getConnectUserId());
                responseRequest.setSuccess(true);
                responseRequest.setData(connectUsers);
                return new ResponseRequest(responseRequest);
            }else{
                ConnectUsers connectUsers2 = connectUsersRepository.findByStatusAndFromsAndTo(status, users1, users);
                if (connectUsers2 != null){
                    connectUsersRepository.deleteRejectUser(connectUsers2.getConnectUserId());
                    responseRequest.setSuccess(true);
                    responseRequest.setData(connectUsers);
                    return new ResponseRequest(responseRequest);
                }
            }
        }else {
            responseRequest.setSuccess(false);
            responseRequest.setStatus("The request is incorrect");
            responseRequest.setData(connectUsers);
            return new ResponseRequest(responseRequest);
        }
        responseRequest.setSuccess(false);
        responseRequest.setStatus("The request has been not sent");
        responseRequest.setData(connectUsers);

        return new ResponseRequest(responseRequest);
    }


}
