var userId = $('span').data('user_id');
var fullNames = $('p').data('full_id');
$(document).ready(function () {
    // CREATE POST ELEMENTS //
    $("#createPost").click(function () {
        $("#exampleModalCenter").show();
        $("#textarea").val('');
        $('.file-name').innerHTML = '';
    });

// POST SAVE //
$(document).on('click',"#post_save",function (event) {
    event.preventDefault();

        var form = $("#formPost")[0];
        var data = new FormData(form);
        // data.append('fullName',fullNames);
        data.append("userId",userId);
        data.append('file',$('.uploaded-file').files);
       if ($('.uploaded-file').files !== null || form !== ""){
           $("#post_save").prop("disabled",true);
           $.ajax({
               type:"POST",
               url:"http://localhost:8082/api/post",
               enctype:"multipart/form-data",
               data:data,
               contentType:false,
               processData: false,
               timeout:6000,
               success:(function (result) {
                   $('#exampleModalCenter').removeClass('show');
                   $('#exampleModalCenter').css('display','none');
                   $('.fade').removeClass('modal-backdrop show');
                   $('body').removeClass('modal-open');
                   $('body').css('padding-right','0');
                   if (result.status === "success") {
                        if (result.data.imgUri !== 0){

                       $(".postLists").before("<div class=\"mt-3 col-md-8 offset-2 bg-white py-2\" style=\"border-radius: 10px;\">\n" +
                           "            <div>\n" +
                           "                <div>\n" +
                           "                    <span>" + result.data.name + "</span>\n" +
                           "                </div>\n" +
                           "                <div>\n" +
                           "                    <span>" + result.data.createAt + "</span>\n" +
                           "                </div>\n" +
                           "            </div>\n" +
                           "            <div style=\"border-radius: 5px; border: 1px solid #ccc; padding: 10px;\">\n" +
                           "                <p>" + result.data.text + "</p>\n" +
                           "                <div>\n" +
                           "                    <div>\n" +
                              "                        <span>\n" +
                              "                            <span>\n" +
                           "<img src='/api/image/"+result.data.imgUri+"'style=\"max-width: 100%; max-height: 400px; \">\n"+
                              "                            </span>\n" +
                              "                        </span>\n"+
                           "                    </div>\n" +
                           "                </div>\n" +
                           "            </div>\n" +
                           "<div data-com_id='"+result.data.postId+"'>\n" +
                           "</div>"+
                           "<div class='w-100'>"+
                           "<div class=\"w-100 d-flex justify-content-end\">\n" +
                           "                    <button data-el_comment_list_button=\""+result.data.postId+"\" class=\"commentList border-0 text-black-50 text-left\" data-toggle=\"modal\" data-target=\"#exampleModalCenter1\">\n" +
                           "                        View comments</button>\n" +
                           "                </div>"+
                           "            <div class=\"d-flex justify-content-between\">\n" +
                           "                <div class=\"mt-3\"><img src=\"/assets/img/staff.jpg\" class=\"image_comment_style m-0\"></div>\n" +
                           "                <div class=\" mt-3\" style=\"width: 96%;\">\n" +
                           "                    <div class=\"ml-2\">\n" +
                           "                        <div  style=\"border-radius: 20px;width: 100%; padding: 5px; background-color:rgba(202,202,202,0.7);\">\n" +
                           "                            <input type=\"text\" name=\"text\" placeholder=\"Fikr bildiring...\" class=\"inputText border-0 w-100\"\n" +
                           "                                   style=\" outline: none; background-color:transparent; padding: 0 6px;\" data-el_id = '"+result.data.postId+"'>\n" +
                           "                        </div>\n" +
                           "                        <span>Yozuv qoldirish uchun enterni bosing</span>\n" +
                           "                    </div>\n" +
                           "\n" +
                           "                </div>\n" +
                           "            </div>\n" +
                           "</div>"+
                           "        </div>");}else {

                            $(".postLists").before("<div class=\"mt-3 col-md-8 offset-2 bg-white py-2\" style=\"border-radius: 10px;\">\n" +
                                "            <div>\n" +
                                "                <div>\n" +
                                "                    <span>" + result.data.name + "</span>\n" +
                                "                </div>\n" +
                                "                <div>\n" +
                                "                    <span>" + result.data.createAt + "</span>\n" +
                                "                </div>\n" +
                                "            </div>\n" +
                                "            <div style=\"border-radius: 5px; border: 1px solid #ccc; padding: 10px;\">\n" +
                                "                <p>" + result.data.text + "</p>\n" +
                                "                <div>\n" +
                                "                    <div>\n" +
                                "                        <span>\n" +
                                "                            <span>\n" +
                                "                            </span>\n" +
                                "                        </span>\n"+
                                "                    </div>\n" +
                                "                </div>\n" +
                                "            </div>\n" +
                                "<div data-com_id='"+result.data.postId+"'>\n" +
                                "</div>"+
                                "<div class='w-100'>"+
                                "<div class=\"w-100 d-flex justify-content-end\">\n" +
                                "                    <button data-el_comment_list_button=\""+result.data.postId+"\" class=\"commentList border-0 text-black-50 text-left\" data-toggle=\"modal\" data-target=\"#exampleModalCenter1\">\n" +
                                "                        View comments</button>\n" +
                                "                </div>"+
                                "            <div class=\"d-flex justify-content-between\">\n" +
                                "                <div class=\"mt-3\"><img src=\"/assets/img/staff.jpg\" class=\"image_comment_style m-0\"></div>\n" +
                                "                <div class=\" mt-3\" style=\"width: 96%;\">\n" +
                                "                    <div class=\"ml-2\">\n" +
                                "                        <div  style=\"border-radius: 20px;width: 100%; padding: 5px; background-color:rgba(202,202,202,0.7);\">\n" +
                                "                            <input type=\"text\" name=\"text\" placeholder=\"Fikr bildiring...\" class=\"inputText border-0 w-100\"\n" +
                                "                                   style=\" outline: none; background-color:transparent; padding: 0 6px;\" data-el_id = '"+result.data.postId+"'>\n" +
                                "                        </div>\n" +
                                "                        <span>Yozuv qoldirish uchun enterni bosing</span>\n" +
                                "                    </div>\n" +
                                "\n" +
                                "                </div>\n" +
                                "            </div>\n" +
                                "</div>"+
                                "        </div>");
                        }
                   }else {
                       alert(result.data.text);
                   }

                   $("#post_save").prop("disabled",false);
               }),
               error:(function (e) {
                   console.log("error: "+ e.responseText);
                   $("#post_save").prop("disabled",false);

               })

           });
       }else {
           alert("ma'lumot to'ldirilmagan.");
       }

    });


    //ADD MY FRIENDS
    $('.userSuggestion').on('click', function (e) {
        var userIdSuggestion= $(this).data('el_id_suggestion');
        e.preventDefault();
        var formSuggestion = {
            'userId': userId,
            'userIdSuggestion': userIdSuggestion,
            'statusValue':30
        };
        $.ajax({
            type: "POST",
            url: "/api/suggestion",
            data: JSON.stringify(formSuggestion),
            dataType: 'json',
            contentType: "application/json",
            cache: false,
            timeout: 6000,
            success: function (result) {
                $('div[data-el_id_suggestion= "' + userIdSuggestion + '"]').addClass('d-none');
                $(".userSuggestion").prop("disabled", false);
                $('.modal-body').append("<div class=\" mt-2\" style=\"display: flex;\" data-el_id_my_friend=\""+result.data.userId+"\">" +
                    "                    <div class=\"w-25 pr-3\">" +
                    "                        <img src=\"/assets/img/staff.jpg\" class=\"px-1 image_comment_style w-100\" style=\"height: 50px;\" alt=\"\">\n" +
                    "                    </div>\n" +
                    "                    <div class=\"w-75\">\n" +
                    "                        <div>\n" +
                    "                            <p class=\"m-0 font-weight-bold\">"+result.data.fullName+"</p>" +
                    "                        </div>\n" +
                    "                        <div>\n" +
                    "                            <form>\n" +
                    "                                <button class=\" disconnectFriendship btn my-2 btn-light w-100\" data-el_id_disconnect=\""+result.data.userId+"\">Olib tashlash</button>\n" +
                    "                            </form>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>");
            },
            error: function (e) {
                console.log(e.responseText);
                $(".userSuggestion").prop("disabled", false);
            }

        });
    });
 //   REJECT SUGGESTION
    $('.rejectSuggestion').on('click', function (e) {
        var userIdSuggestion= $(this).data('el_id_suggestion');
        e.preventDefault();
        var formSuggestion = {
            'userId': userId,
            'userIdSuggestion': userIdSuggestion,
            'statusValue':60
        };
        $.ajax({
            type: "POST",
            url: "/api/suggestion",
            data: JSON.stringify(formSuggestion),
            dataType: 'json',
            contentType: "application/json",
            cache: false,
            timeout: 6000,
            success: function (result) {
                console.log(result);

                $('div[data-el_id_suggestion= "' + userIdSuggestion + '"]').addClass('d-none');
                $(".userSuggestion").prop("disabled", false);
            },
            error: function (e) {
                console.log(e.responseText);
                $(".userSuggestion").prop("disabled", false);
            }

        });
    });

 });
//--- Comment save js ---//
$(document).on('keypress',".inputText", function (e) {

    var keyCode = (e.keyCode ? e.keyCode : e.which);
    if(keyCode === 13) {
        var id = $(this).data('el_id');
        var textValue = $(this).val();
        $(function() {
            var infoAll = {
                "postId":id,
                "fullNames": fullNames,
                "text":textValue,
                "userId":userId
            };
            $.ajax({
                type:"POST",
                url:"/api/commentSave",
                data:JSON.stringify(infoAll),
                dataType:'json',
                contentType:"application/json",
                cache:false,
                timeout:6000,
                success:(function (result) {
                    if (result.status === "success") {
                        $('div').data('new_com_id',id);
                        $('div[data-new_com_id= "'+id+'"]').append(
                            "<div class=\"w-100 \" >" +
                            "                    <div >" +
                            "                        <div class=\"d-inline-block mt-3 p-2 px-4\" style=\"border-radius: 25px;background-color:rgba(202,202,202,0.7);\">" +
                            "                           <div class='d-flex'>"+
                            "                               <img src='/assets/img/staff.jpg' class='image_comment_style'>"+
                            "                               <div class='d-inline-block'>"+
                            "                                   <p class='my-0 font-weight-bold' >"+result.data.fullName+"</p>"+
                            "                                   <span >"+result.data.date+"</span>"+
                            "                               </div>"+
                            "                            </div>"+
                            "                            <p class=\"my-0\">"+result.data.textValue+"</p>" +
                            "                        </div>" +
                            "                    </div>" +
                            "                </div>"
                        );
                    }
                }),
                error:(function (e) {
                    console.log("ERROR: "+e);
                })
            });
        });
        $(this).val('');
    }
});


// SUGGESTION FRIEND
$(document).on('click', '.suggestionUser', function (e) {
    var userIdSuggestion= $(this).data('el_user_id_suggestion');
    e.preventDefault();
    var formSuggestion = {
        'userId': userId,
        'userIdSuggestion': userIdSuggestion,
        'statusValue':10
    };

    $.ajax({
        type: "POST",
        url: "/api/suggestion",
        data: JSON.stringify(formSuggestion),
        dataType: 'json',
        contentType: "application/json",
        cache: false,
        timeout: 6000,
        success: function (result) {
            if (result.success) {
                console.log(result.success);
                $('div[data-el_user_id= "' + userIdSuggestion + '"]').addClass('d-none');
                $('body').append("<div class='position-fixed text-white' style='border-radius: 5px; top: 66px;right: 0px; background-color:#0c6000; z-index: 1000; width: 240px; height: 70px; padding: 10px;'>" +
                    "   <p class='m-0'>"+result.status+"</p>" +

                    "</div>");
                $(".suggestionUser").prop("disabled", false);
            }else {
                $('body').append("<div class='position-fixed text-white' style=' border-radius: 5px; top: 66px;right: 0px; background-color:#c00808; z-index: 1000; width: 240px; height: 70px; padding: 10px;'>" +
                    "   <p class='m-0'>"+result.status+"</p>" +

                    "</div>");
            }
        },
        error: function (e) {
            console.log(e.responseText);
            $(".suggestionUser").prop("disabled", false);
        }

    });
});

// CANCEL THE REQUEST
$(document).on('click', '.removalUser', function (e) {
    var userIdSuggestion = $(this).data('el_user_id_removal_user');
    e.preventDefault();

    var formSuggestion = {
        'userId': userId,
        'userIdSuggestion': userIdSuggestion,
        'statusValue': 12
    };

    $.ajax({
        type: "POST",
        url: "/api/suggestion",
        data: JSON.stringify(formSuggestion),
        dataType: 'json',
        contentType: "application/json",
        cache: false,
        timeout: 6000,
        success: function (result) {
            if (result.success) {
                console.log(result.success);
                $('div[data-el_user_id= "' + userIdSuggestion + '"]').addClass('d-none');
                $('body').append("<div class='position-fixed text-white' style='border-radius: 5px; top: 66px;right: 0px; background-color:#0c6000; z-index: 1000; width: 240px; height: 70px; padding: 10px;'>" +
                    "   <p class='m-0'>" + result.status + "</p>" +
                    "</div>");
                $(".removalUser").prop("disabled", false);
            } else {
                $('body').append("<div class='position-fixed text-white' style=' border-radius: 5px; top: 66px;right: 0px; background-color:#c00808; z-index: 1000; width: 240px; height: 70px; padding: 10px;'>" +
                    "   <p class='m-0'>" + result.status + "</p>" +

                    "</div>");
            }
        },
        error: function (e) {
            console.log(e.responseText);
            $(".removalUser").prop("disabled", false);
        }

    });
});
//   DISCONNECT FRIENDSHIP
$(document).on('click','.disconnectFriendship',function (e) {
    var userIdSuggestion= $(this).data('el_id_disconnect');
    e.preventDefault();
    var formSuggestion = {
        'userId': userId,
        'userIdSuggestion': userIdSuggestion,
        'statusValue':70
    };
    $.ajax({
        type: "POST",
        url: "/api/suggestion",
        data: JSON.stringify(formSuggestion),
        dataType: 'json',
        contentType: "application/json",
        cache: false,
        timeout: 6000,
        success: function (result) {
            if (result.success) {
                $('div[data-el_id_my_friend= "' + userIdSuggestion + '"]').addClass('d-none');
                $(".disconnectFriendship").prop("disabled", false);
            }
        },
        error: function (e) {
            console.log(e.responseText);
            $(".disconnectFriendship").prop("disabled", false);
        }
    });
});
// <-- LIST OF COMMENTS IN THE POST -->
var i = 0;
$(document).on('click','.commentList',function (e) {
    $('.newComment').addClass('d-none');
    if (i === 0){
        i++;
    } else {
        i--;
        $('.newComment').removeClass('d-none');
        $('.newComment').empty();
    }
    var buttonDataEl = $(this).data('el_comment_list_button');
    e.preventDefault();
    var data = new FormData();
    data.append('postId',buttonDataEl);
    $.ajax({
            type:"POST",
            url:'api/postCommentAll',
            enctype:'multipart/form-data',
            data:data,
            contentType:false,
            processData:false,
            timeout:6000,
            success: function (result) {
            if (result.success) {
                $('div[data-com_id= "'+buttonDataEl+'"]').empty();
                for (var i = 0; i < result.comments.length; i++) {
                    if (buttonDataEl === result.comments[i].post.postId) {
                        $('div[data-com_id= "'+buttonDataEl+'"]').append("<div class=\"w-100 \">\n" +
                            "                        <div>\n" +
                            "                            <div class=\"d-inline-block mt-3 p-2 px-4\" style=\"border-radius: 25px;background-color:rgba(202,202,202,0.7);\">\n" +
                            "                                <div class=\"d-flex\">\n" +
                            "                                    <img src=\"/assets/img/staff.jpg\" class=\"image_comment_style\">\n" +
                            "                                    <div class=\"d-inline-block\">\n" +
                            "                                        <p class=\"my-0 font-weight-bold\" >" + result.comments[i].user.fullName + "</p>\n" +
                            "                                        <span>" + result.comments[i].createAt + "</span>\n" +
                            "                                    </div>\n" +
                            "                                </div>\n" +
                            "                                <p class=\"my-0\">" + result.comments[i].text + "</p>\n" +
                            "                            </div>\n" +
                            "\n" +
                            "                        </div>\n" +
                            "                    </div>");
                    }
                }
            } else {
                $('div[data-com_id= "'+buttonDataEl+'"]').empty();
                $('div[data-com_id= "'+buttonDataEl+'"]').append("No comments");
            }
            $(".commentList").prop("disabled", false);

        },
        error:function (e) {
            console.log(e);
            $(".commentList").prop("disabled", false);

        }
    });

});